﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS11_ParameterArrays
{
    class NewSolutionParams
    {
        //only params keyword added
        public static int Min(params int[] paramList)
        {
            if (paramList == null || paramList.Length == 0)
            {
                throw new Exception("Not enough params.");
            }

            int currentMin = paramList[0];

            foreach (var item in paramList)
            {
                if (item < currentMin)
                {
                    currentMin = item;
                }
            }

            return currentMin;
        }

        //Result: Compiler creates array for us. - it's less code.
        //Remember - params must be the last parameter

        //But - what if we want to pass different types of parameters?
    }
}
