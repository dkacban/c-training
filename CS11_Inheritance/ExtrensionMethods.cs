﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS12_Inheritance
{
    static class ExtrensionMethods // tylko statyczne klasy
    {
        public static int Negate(this int i)
        {
            return -i;
        }

        public static String DisplayUpperLetters(this String i)
        {
            return i.ToUpper();
        }
    }
}
