﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS10_Arrays
{
    class Array_2
    {
        //plural names
        private DateTime[] dates;
        //Array are reference types? int[] również?


        //all elements have the same type
        //elements placed in the contiguous block in memory
        //elements accessed by index
        public int CalculateSum()
        {
            //data allocation? Only pointer - because it's reference type
            int[] numbersTest;       
            //Instance of an array
            //in the following line we're allocating memory on the heap
            //size can be 0
            //default values - zeros for all numeric data types
            numbersTest = new int[6];

            //initialize an array:
            //int[] numbers = new int[6] {10, 10, 10, 11, 11, 11};
            

            Random r = new Random();
            int[] numbers = new int[3] { r.Next() % 10, r.Next() % 10, r.Next() % 10 };

            //This is also correct:
            int[] numbers2 = { r.Next() % 10, r.Next() % 10, r.Next() % 10 };

            var result = 0;

            for (int i = 0; i < numbers.Length; i++)
            {
                Console.WriteLine(numbers[i]);
            } 

            foreach (int number in numbers)
            {
                result += number;
            }

            return result;
        }

        public void ImplicitlyTypedArray()
        {
            var names = new[] {"darek", "jarek", "jan"};

            //compile time error - types must be consistent - avoid mixing types
            //var bad = new[] { "John", "Diana", 99, 100 };
        }

        public void ArrayOfAnonymousTypes()
        {
            //types must be the same for all array
            var names = new[] { new { Name = "John", Age = 47 }, 
                                new { Name = "Diana", Age = 46 }, 
                                new { Name = "James", Age = 20 }, 
                                new { Name = "Francesca", Age = 18 } };            
        }

        public void AccessIndividualElements()
        {
            //arrays are zero index!
            int[] players = {8, 22, 15, 33, 56};
            Console.WriteLine(players[0]);
            Console.WriteLine(players[4]);
        }

        //passing array
        public void ProcessData(int[] data)
        {
            foreach (int i in data)
            {
                Console.Write(i);
            }
        }

        public int[] ReadData()
        {
            Console.WriteLine("How many elements?"); 
            string reply = Console.ReadLine(); 
            int numElements = int.Parse(reply);
            int[] data = new int[numElements];

            for (int i = 0; i < numElements; i++)
            {
                Console.WriteLine("Enter data for element {0}", i); 
                reply = Console.ReadLine(); 
                int elementData = int.Parse(reply); 
                data[i] = elementData;
            } 
            
            return data;
        }

        //Check System.Array class methods: Copy, CopyTy, Sort

        public void MultiDimensionalArrays()
        {
            //2 dimensions: 4 rows, 6 columns
            int[,] items = new int[4, 6];

            //3 dimensions
            int[, ,] cube = new int[5, 5, 5]; cube[1, 2, 1] = 101; cube[1, 2, 2] = cube[1, 2, 1] * 3;
        }
    }
}
