﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS12_Inheritance
{
    class Program
    {
        /*
         * Creating derived class
         * New, Virtual, override keywords
         * Using protected keyword
         * Defining Extension method
         */ 


        static void Main(string[] args)
        {
        }
        //1. Inheritance:
        //Write class Building
        //Add props: Height, Price
        //Add constructor to set props
        //Add Methods: ShowHeight, ShowPrice to it
        //Create class House
        //Add props: Height, Price
        //Add constructor to set props
        //Add Methods: ShowHeight, ShowPrice to it
        //Create class Flat
        //Add props: Height, Price
        //Add constructor to set props
        //Add Methods: ShowHeight, ShowPrice to it
        //Improve

        //2. parent class of all classes.

        //3. Calling base constructor for house

        //4. Assigning classes
        //Create flat object and assign it to flat variable
        //Create flat object and assign it to house variable
        //Create flat object and assign it to building variable

        //5. Hiding method - warning message
        //Create 2 classes(house, flat), declare 2 methods(ShowHeight) with the same signature in them(but different body)
        //Create SingleFlat that derives from flat and call method
        //Use new keyword to ignore warning

        //6. Using virtual keyword - to mark method that needs to be overriden in sbclass

        //7. Using override keyword
        //create building object with method GetHeight and return 10
        //create house object
        //create flat object
        //building = Building
        //building.GetHeight()
        //building = Flat
        //building.GetHeight()
        //building = SingleFlat
        //houbuildingse.GetHeight()

        //8. Protected keyword
        //create public method in a building class
        //Try to use it in house class
        //call from main method
        //create protected method in a building class
        //Try to use it in house class
        //call from main method
 
        //9.Extensions methods
    }
}
