﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS11_ParameterArrays
{
    class SolutionArrays
    {
        public static int Min(int[] paramList)
        {
            if (paramList == null || paramList.Length == 0)
            {
                throw new Exception("Not enough params.");
            }

            int currentMin = paramList[0];

            foreach (var item in paramList)
            {
                if (item < currentMin)
                {
                    currentMin = item;
                }
            }

            return currentMin;
        }
    }

    //call: 

    //int first = 10;
    //int second = 2;
    //int third = 5
    //int min = Util.Min(new int[] {first, second, third});

    //result - no need to create many overloads but need to write additional code to construct array
}
