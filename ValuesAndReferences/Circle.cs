﻿namespace CS8_ValuesAndReferences
{
    public partial class Circle
    {
        int radius;

        public Circle()
        {
        }

        public Circle(int radius)
        {
            this.radius = radius;
        }

        public void SetRadius(int radius)
        {
            this.radius = radius;
        }
    }
}
