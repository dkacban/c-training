﻿using System.Runtime.Remoting.Messaging;

namespace CS10_Arrays
{
    public class SingleVariables1
    {
        public int CalculateSum()
        {
            int number1 = 10;
            int number2 = 10;
            int number3 = 10;
            int number4 = 11;
            var number5 = 11;
            var number6 = 11;

            int result = number1 + number2 + number3 + number4 + number5 + number6;

            return result;
        }
        //potential problems?
        //What if we want to declare 100 elements?

    }
}