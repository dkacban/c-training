﻿using System;
using System.Text.RegularExpressions;

namespace CS8_ValuesAndReferences
{
    class Program
    {
        static void Main(string[] args)
        {
            RefHowWorks();
        }

        private static void CopyValues()
        {
            int number = 15;
            int numberCopy = number;
            Console.WriteLine(number);
            Console.WriteLine(numberCopy);

            number++;
            Console.WriteLine(number);
            Console.WriteLine(numberCopy);
        }

        private static void CopyReferences()
        {
            Circle circle = new Circle(10);
            Circle circleCopy = circle;
            Console.WriteLine(circle.GetArea()); 
            Console.WriteLine(circle.GetArea());

            circle.SetRadius(160);

            Console.WriteLine(circle.GetArea()); 
            Console.WriteLine(circle.GetArea());
        }

        private static void CheckNull()
        {
            //assign nothing would be a bad practice:
            Circle circle = null;

            if (circle == null)
            {
                circle = new Circle(11);
            }

            Console.WriteLine(circle.GetPerimeter());
        }

        private static void NullableTypes()
        {
            int a = 10;
            //int i = null; - incorrect
            int? i = null;
            //a = i; - incorrect
            i = 16;
            //2 properties of nullable types:
            if(!i.HasValue)
            {
                Console.WriteLine(i.Value);//Value is read only
            }
        }

        private static void NoRefHowWorks()
        {
            int number = 50;
            DoIncrement(number);
            Console.WriteLine(number); // 50 or 51?
        }

        private static void DoIncrement(int val)
        {
            val++;
        }

        private static void RefHowWorks()
        {
            int number = 50;
            //in this line number must be initialized
            DoIncrement(ref number);
            Console.WriteLine(number);
        }

        private static void DoIncrement(ref int val)
        {
            val++;
        }

        private static void OutHowWorks()
        {
            int number;
            //in this line number musN'T be initialized
            DoIncrementOut(out number);
            Console.WriteLine(number);
        }

        private static void DoIncrementOut(out int val)
        {
            val = 0; //out params MUST be assigned in the method
        }
    }
}
