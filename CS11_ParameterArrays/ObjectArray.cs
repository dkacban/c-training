﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS11_ParameterArrays
{
    class ObjectArray
    {
        public static void DisplayAll(params object[] paramList)
        {
            if (paramList == null || paramList.Length == 0)
            {
                throw new Exception("Not enough params.");
            }

            foreach (var item in paramList)
            {
                Console.WriteLine(item);
            }
        }
    }
}
